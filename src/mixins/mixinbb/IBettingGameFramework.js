export default {
    data() {
        return {
            game_run_time: {}
        }
    },
    methods: {
        clearData() {
        },
        pushFactor(a, b) {
        },
        startJump() {
        },
        kill() {
        },
        start_game_render() {
        }
    },
    mounted() {
        this.$nextTick(() => {
            this.start_game_render()
        });
    },
    updated() {
    },
    destroyed() {
    }
}