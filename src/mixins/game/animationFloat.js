import {TimelineLite, Back, Power2, Power1} from "gsap";

export default {
    data() {
        return {
            float_cached_val: 0.000000,
            float_x_timer: 0,
            float_animate_precision: 0.0001,
        }
    },
    mounted() {
        clearInterval(this.float_x_timer);
    },
    destroyed() {
        clearInterval(this.float_x_timer);
    },
    methods: {
        UpdateFunction(currentVal) {

        },
        DoneFunction(end_value) {

        },
        animateValue(oldVal, newValue, duration) {
            this.$GSLite.tweenFromTo(oldVal, newValue, Power1).duration(2);
            this.$GSLite.eventCallback("onComplete", this.DoneFunction, [newValue]);
        },

    }
}