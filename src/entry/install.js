import Aurora from './../components/bg_aurora'
import Smoke from './../components/bg_smoke'
import Coalesce from './../components/bg_coalesce'
import Pipeline from './../components/bg_pipeline'
import Shift from './../components/bg_shift'

export function install(Vue, settings) {
    [Aurora, Smoke, Coalesce, Pipeline, Shift].forEach(comp => {
        Vue.component(comp.name, comp)
    })
}

export {
    Aurora,
    Smoke,
    Coalesce,
    Pipeline,
    Shift
}

export default install