import Vue from "vue";

/*

import lodash from "lodash";
Object.defineProperty(Vue.prototype, "$L", { value: lodash });
*/

//must have this
export const WEBSOCKSW = {
    active: false,
    heartbeat: false
};

export const EventBus = new Vue();